__all__ = ['SEAMTower']

from math import pow
import numpy as np
from scipy.optimize import minimize

from openmdao.main.api import Component
from openmdao.lib.datatypes.api import Float, Array


class SEAMTower(Component):
    """
    """

    rho_steel = Float(7.8e3, iotype='in', units='kg/m**3', desc='density of steel')
    hub_height = Float(iotype='in', units='m', desc='Hub height')
    tower_bottom_diameter = Float(iotype='in', units='m', desc='Tower bottom diameter')
    tower_top_diameter = Float(iotype='in', units='m', desc='Tower top diameter')

    lifetime_cycles = Float(1.e7, iotype='in', desc='Equivalent lifetime cycles')
    tower_bottom_moment_max = Float(iotype='in', units='kN*m', desc='Max tower bottom moment')
    tower_bottom_moment_leq = Float(iotype='in',  units='kN*m', desc='Lifetime equivalent tower bottom moment')
    stress_limit_extreme_tower = Float(iotype='in', units='MPa', desc='Tower ultimate strength')
    stress_limit_fatigue_tower = Float(iotype='in', units='MPa', desc='Tower fatigue strength')
    safety_factor_tower = Float(iotype='in', desc='Tower loads safety factor')
    PMtarget_tower = Float(1., iotype='in', desc='')
    wohler_exponent_tower = Float(4., iotype='in', desc='Tower fatigue Wohler exponent')

    tower_z = Array(iotype='out', desc='Tower discretization')
    tower_wall_thickness = Array(iotype='out', units='m', desc='Tower wall thickness')
    tower_mass = Float(iotype='out', units='kg', desc='Tower mass')

    def __init__(self, n_sections):
        super(SEAMTower, self).__init__()

        self.n_sections = n_sections
        self.tower_wall_thickness = np.zeros(n_sections)

    def execute(self):
        """ calculate tower wall thickness and total mass """

        top_extreme = self.tower_bottom_moment_max / 4.  # Assumes 1/4 in top
        bott_extreme = self.tower_bottom_moment_max
        top_fatigue = self.tower_bottom_moment_leq / 6.  # Assumes 1/6 in top
        bott_fatigue = self.tower_bottom_moment_leq

        h = np.linspace(0, self.hub_height, self.n_sections)
        D = np.linspace(self.tower_bottom_diameter, self.tower_top_diameter, self.n_sections)

        self.Mext = np.linspace(bott_extreme, top_extreme, self.n_sections)
        self.Meq = np.linspace(bott_fatigue, top_fatigue, self.n_sections)

        text = 1000. * self.Mext * 4./ (np.pi * (self.stress_limit_extreme_tower / self.safety_factor_tower * 1.e6) * D**2)

        tfat = np.zeros(self.n_sections)
        for i in range(self.n_sections):
            res = minimize(self._solve, 0.05, args=(D[i], self.Meq[i]),
                           bounds=[(1.e-6, 0.2)], method='SLSQP', tol=1.e-8)
            tfat[i] = res['x']

            self.tower_wall_thickness[i] = np.maximum(tfat[i], text[i])

        # calculate volume: 2 * pi * r * dr
        dvol = np.pi * (D - self.tower_wall_thickness) * self.tower_wall_thickness
        vol = np.trapz(dvol, h)
        self.tower_mass = self.rho_steel * vol
        self.tfat = tfat
        self.text = text
        self.tower_z = h

    def _solve(self, t, D, Meq):

        W = (np.pi / 32.) * (D**4 - (D - 2. * t)**4) / D
        sfat = self.safety_factor_tower * 1.e3 * Meq/ W / 1.e6
        PM = self.lifetime_cycles / (pow(10, (self.stress_limit_fatigue_tower - self.wohler_exponent_tower*np.log10(sfat))))
        return abs(PM - self.PMtarget_tower)

    def plot(self, fig):
        """
        function to generate Bokeh plot for web GUI.

        Also callable from an ipython notebook

        parameters
        ----------
        fig: object
            Bokeh bokeh.plotting.figure object

        returns
        -------
        fig: object
            Bokeh bokeh.plotting.figure object
        """
        try:
            # formatting
            fig.title = 'Tower wall thickness'
            fig.xaxis[0].axis_label = 'Height [m]'
            fig.yaxis[0].axis_label = 'Thickness [m]'

            # fatigue, ultimate and final thickness line plots
            fig.line(self.tower_z, self.tfat, line_color='orange',
                                        line_width=2,
                                        line_dash=[4, 4],
                                        legend='Fatigue')
            fig.line(self.tower_z, self.text, line_color='green',
                                        line_width=2,
                                        line_dash=[4, 4],
                                        legend='Ultimate')
            fig.line(self.tower_z, self.t, line_color='blue',
                                     line_width=2,
                                     legend='Final thickness')

            fig.legend[0].orientation = 'bottom_left'
        except:
            pass

        return fig
